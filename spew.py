# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 10:26:08 2015

@author: mttang
"""

import sys
from time import sleep
from PyQt4 import QtCore, QtGui, uic
from requests import Session
import json
import qdarkstyle
import os

qtCreatorFile = "newt3.ui"
string = "20130713_185717"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)
s = Session()
newt_base_url = "https://newt.nersc.gov/newt"
my_data = {}
derived_data = {}
derived_list = []
dir_base = "/global/project/projectdirs/als/spade/warehouse"
json_data = []
web_directory = ""
scratch_number = ["scratch1","scratch2","scratch3","scratch4","scratch5","scratch6","scratch7","scratch8","scratch9"]
scratch_root = ""
command = ""
stop_download = False
taskCompleted = False
indexItem = None
rootpath = os.path.split(os.path.abspath("newt3.ui"))[0]+'\\'

"""
Class for all the functions used for running simple GUI actions
"""
class Tasks(QtCore.QObject):
    
    workingMessage = QtCore.pyqtSignal(str)
    clearSCRATCHSignal = QtCore.pyqtSignal()
    clearLOCALSignal = QtCore.pyqtSignal()
    clearSPOTSignal = QtCore.pyqtSignal()
    fillTableSignal = QtCore.pyqtSignal(object, object)
    fillTreeSignal = QtCore.pyqtSignal(object,object)
    fillLocalSignal = QtCore.pyqtSignal()
    taskDone = QtCore.pyqtSignal()
    taskStart = QtCore.pyqtSignal()
    downloadSignal = QtCore.pyqtSignal(int)
    percentSignal = QtCore.pyqtSignal(int)

    def __init__ (self, parent):
        QtCore.QObject.__init__(self)
        self.parent = parent
    
    def handle_login(self):
        self.taskStart.emit()
        self.workingMessage.emit("Logging In...")

        print "[%s] Processing handleLogin" % QtCore.QThread.currentThread().objectName()
        if (window.user_box.text() != '' and
            window.pass_box.text() != ''):
            r = s.post("https://portal-auth.nersc.gov/als/auth", {"username": str(window.user_box.text()), "password": str(window.pass_box.text())})
            r = s.post("https://newt.nersc.gov/newt/auth", {"username": str(window.user_box.text()), "password": str(window.pass_box.text())})
            response = r.json()
            if (response['auth']):
                self.workingMessage.emit("Logged in succesfully")
                window.console.setText(r.text)
            else:
                self.workingMessage.emit("Wrong Username or Password!")
        self.taskDone.emit()
    def cancel_process(self):
        print "Cancel Process"
        global stop_download
        if (stop_download == False):
            stop_download = True
        else: 
            print "Nothing to cancel!"
        print stop_download
        
    def NERSC_process(self):
        global scratch_root
        print "[%s] Processing handleProcess" % QtCore.QThread.currentThread().objectName()

        if (window.operation_box.currentText() == "Copy this SPOT dataset to my SCRATCH"):
            self.taskStart.emit()

            print "Copying dataset to SCRATCH!"
            self.workingMessage.emit("Copying dataset to SCRATCH!")

            item = window.spot_data_widget.currentItem()
            filename = item.text(0)
            print filename
            if (item == None):
                self.workingMessage.emit("No dataset selected!")
                self.taskDone.emit()
                return
            #print filename
            for key in my_data.keys():
                #print key
                if key in filename and '/als/' not in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['raw']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['raw']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)
                    
                if key in filename and '/raw/' in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['raw']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['raw']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)
                    
                if key in filename and '/norm/' in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['norm']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['norm']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)
                    
                if key in filename and '/gridrec/' in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['gridrec']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['gridrec']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)

                if key in filename and '/imgrec/' in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['imgrec']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['imgrec']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)

                if key in filename and '/rc/' in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['rc']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['rc']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)

                if key in filename and '/sino/' in filename:
                    print "executable:"+ "/bin/cp -avr "+dir_base+my_data[key]['sino']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())
                    s.post(newt_base_url+"/command/edison", {"executable": "/bin/cp -avr "+dir_base+my_data[key]['sino']+" /"+scratch_root+"/scratchdirs/"+str(window.user_box.text())})
                    print "Transfer complete"
                    self.cdls(path=web_directory, widget=window.scratch_data_widget)
                    
            self.taskDone.emit()
            self.workingMessage.emit("Finished copying dataset!")

        if (window.operation_box.currentText() == "Delete Dataset from SCRATCH"):
            self.taskStart.emit()

            print "Deleting dataset from SCRATCH!"
            self.workingMessage.emit("Deleting Dataset from SCRATCH!")

            item = window.scratch_data_widget.currentItem()
            if (item == None):
                self.workingMessage.emit("No dataset selected!")
                self.taskDone.emit()
                return
            filename = item.text()
            filename = str(filename)
            #print filename
            #print web_directory
            real_directory = web_directory[12:]
            print "executable:"+ "/bin/rm -rf "+real_directory+'/'+filename
            if ('.h5' in filename):
                s.post(newt_base_url+"/command/edison", {"executable": "/bin/rm -rf "+real_directory+'/'+filename})
                print "File Deleted"
            self.cdls(path=web_directory, widget=window.scratch_data_widget)
            self.taskDone.emit()
            self.clearLOCALSignal.emit()

            self.workingMessage.emit("File Deleted")
            
    def cdls(self,path="", widget=None):
        global web_directory 
        if (path == "/file"):
            web_directory += "/edison"
        print "web_directory = "+web_directory
        self.clearSCRATCHSignal.emit()
        self.taskStart.emit()
        r = s.get(newt_base_url + web_directory)
        directory_items = r.json()
        for item in directory_items:
            if (item['name'] != '.'):
               widget.addItem(item['name'])
        directory_items = None
        self.taskDone.emit()
        
    def handleSCRATCHDirectoryList(self):
        global scratch_root
        self.taskStart.emit()
        print "[%s] Processing handleSCRATCHDirectoryList" % QtCore.QThread.currentThread().objectName()
        self.workingMessage.emit("Loading SCRATCH directory")
        global web_directory
        foundScratch = False
        for rootfolder in scratch_number:
            r = s.get(newt_base_url + "/file/edison/"+rootfolder+"/scratchdirs/"+str(window.user_box.text()))
            for key in r.json():          
                for value in key:
                    if value == "group":
                        foundScratch = True
                        scratch_root = rootfolder
                        web_directory =  "/file/edison/"+rootfolder+"/scratchdirs/"+str(window.user_box.text())
                        break
                if (foundScratch == True):
                    break    
        if foundScratch == False:
            self.workingMessage.emit("SCRATCH directory not found!")
            self.taskDone.emit()
            return
        self.cdls(path=web_directory, widget=window.scratch_data_widget)
        self.workingMessage.emit("SCRATCH directory loaded!")
        self.taskDone.emit()
    
    def handleSCRATCHAction(self):
        print "[%s] Processing handleSCRATCHAction" % QtCore.QThread.currentThread().objectName()
        global web_directory
        item = window.scratch_data_widget.currentItem()
        qfile = item.text()
        filename = str(qfile)
        if len(filename.split('.')) == 1:
            web_directory = web_directory+'/'+filename
            #print web_directory
            self.cdls(path=web_directory, widget=window.scratch_data_widget)
        elif filename == "..":
            web_directory = web_directory[:web_directory.rindex('/')]
            #print web_directory
            self.cdls(path=web_directory, widget=window.scratch_data_widget)
        elif ".h5" in filename:
            print filename + " is an h5 file!"
                
    def handleSpotSearchRequest(self):
        self.taskStart.emit()
        print "[%s] Processing handleSpotSearchRequest" % QtCore.QThread.currentThread().objectName()
        self.workingMessage.emit("Searching for SPOT datasets")

        searchParams = {'skipnum': '0'}
        searchParams.update({'limitnum': str(window.limit_box.text())})
        if (window.sort_box.currentText() == "Processed Date"):
            searchParams.update({'sortterm': 'fs.stage_date'})
        else:
            searchParams.update({'sortterm': 'appmetadata.sdate'})
        if (window.order_box.currentText() == "Descending"):
            searchParams.update({'sorttype': 'desc'})
        else:
            searchParams.update({'sorttype': 'asc'})
        searchParams.update({'search': str(window.search_box.text())})
        searchParams.update({'end_station': 'bl832'})
        r = s.get("https://portal-auth.nersc.gov/als/hdf/search", params=searchParams)
        if (r.content == "[]"):
            self.clearSPOTSignal.emit()
            self.taskDone.emit()
            self.workingMessage.emit("No results from SPOT Search!")
            return
        jstr = r.json()
        with open('spotdata.json', 'wb') as outfile:
            json.dump(jstr, outfile, sort_keys = True, indent = 4, ensure_ascii=False)  
        self.taskDone.emit()
        self.workingMessage.emit("SPOT Search finished!")

        self.handleTreeView()

    def handleTreeView(self):
        print "[%s] Processing handleTreeView" % QtCore.QThread.currentThread().objectName()
        self.clearSPOTSignal.emit()
        if (len(my_data) > 0):
            my_data.clear()
        
        derived_list=[]
        derived_data={}
            
        with open("spotdata.json") as json_file:
            json_data = json.load(json_file)
            for index in range(len(json_data)):
                
                derived_data = {
                    json_data[index]['fs']['stage'] : json_data[index]['fs']['path']
                }
                derived_list.append(derived_data)
                for keys in json_data[index]['fs']:
                    if 'derivatives' in keys:
                        for d_index in range(len(json_data[index]['fs']['derivatives'])):
                            derived_data[json_data[index]['fs']['derivatives'][d_index]['dstage']] = json_data[index]['fs']['derivatives'][d_index]['did']
                
                my_data[json_data[index]['fs']['dataset']] = derived_list[index]

            with open('spotdata2.json', 'wb') as outfile:
                json.dump(my_data, outfile, sort_keys = True, indent = 4, ensure_ascii=False)                   
            #self.fill_treeWidget(window.spot_data_widget, my_data)
            self.fillTreeSignal.emit(window.spot_data_widget, my_data)
            json_data=None
            #print len(my_data)
        window.spot_data_widget.show()  
        
    def handleTableView(self):
        print "[%s] Processing handleTableView" % QtCore.QThread.currentThread().objectName()

        item = window.spot_data_widget.currentItem()
        filename = item.text(0)
        #print filename
        for key in my_data.keys():
            if key in filename:
                #print key
                #print "https://portal-auth.nersc.gov/als/hdf/attributes"+my_data[key]['raw']
                r = s.get("https://portal-auth.nersc.gov/als/hdf/attributes"+my_data[key]['raw'])
                j = r.json()
                #print j
                self.fillTableSignal.emit(window.spot_meta_widget,j) 
                #self.fill_tableWidget(window.spot_meta_widget, j)

"""
Class for the GUI; This is the main thread. Most functions here simply emit a signal to be processed in another thread, thus
the main thread won't hang while fetching or sending data to NERSC.
"""
class MyApp(QtGui.QWidget, Ui_MainWindow):
    
    directUploadSignal = QtCore.pyqtSignal(str,str) #use to upload scripts automatically to run tomopy/crop
    
    def __init__(self):
        QtGui.QWidget.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.imageLabel.setScaledContents(False)
        self.imageScrollArea.setWidget(self.imageLabel)
        self.buildLocalDirectoryView()
        self.tabWidget.setCurrentIndex(0)
        self.worker = Worker(self)
        self.downloader = Downloader(self)
        self.uploader = Uploader(self)
        self.remoteSH = RemoteSH(self)

        self.thread = QtCore.QThread(self, objectName='workerThread')
        self.downloadThread = QtCore.QThread(self, objectName='downloaderThread')
        self.uploadThread = QtCore.QThread(self, objectName='uploadThread')
        self.shThread = QtCore.QThread(self, objectName='shThread')

        self.worker.moveToThread(self.thread)
        self.downloader.moveToThread(self.downloadThread)
        self.uploader.moveToThread(self.uploadThread)
        self.remoteSH.moveToThread(self.shThread)

        self.thread.started.connect(self.worker.start)
        self.downloadThread.started.connect(self.downloader.start)
        self.uploadThread.started.connect(self.uploader.start)
        self.shThread.started.connect(self.remoteSH.start)

        self.thread.start()
        self.downloadThread.start()
        self.uploadThread.start()
        self.shThread.start()

        self.login_button.clicked.connect(self.handleLogin)
        self.start_process_button.clicked.connect(self.execute_NERSC_process)
        self.cancel_process_button.clicked.connect(self.cancel_process)
        self.console.textChanged.connect(self.handleSCRATCHDirectoryList)
        self.console.textChanged.connect(self.handleSpotSearchRequest)
        self.search_box.returnPressed.connect(self.handleSpotSearchRequest)
        self.spot_data_widget.itemExpanded.connect(self.expandColumn)
        self.spot_data_widget.itemClicked.connect(self.handleTableView)
        self.scratch_data_widget.itemClicked.connect(self.handleSCRATCHAction)

    def buildLocalDirectoryView(self):
        self.fileSystemModel = QtGui.QFileSystemModel(self.local_data_view)
        self.fileSystemModel.setReadOnly(False)
        root = self.fileSystemModel.setRootPath("/")
        self.local_data_view.setModel(self.fileSystemModel)
        self.local_data_view.setRootIndex(root)       
        self.local_data_view.clicked.connect(self.on_treeView_clicked)

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def on_treeView_clicked(self, index):
        global indexItem
        indexItem = self.fileSystemModel.index(index.row(), 0, index.parent())
        fileName = self.fileSystemModel.fileName(indexItem)
        print fileName
        filePath = self.fileSystemModel.filePath(indexItem)
        print filePath
        
    def onStart(self): 
        self.progressBar.setRange(0,0)

    def onFinished(self):
        # Stop the pulsation
        self.progressBar.setRange(0,1)
    
    def setDownloadRange(self, i):
        self.progressBar.setRange(0,100)    
    
    def onProgress(self, i):
        self.progressBar.setValue(i)
        
    @QtCore.pyqtSlot(str)
    def printMessage(self, results):
        if "%" in results:
            self.percentLabel.setText(results)
        else:
            self.console2.setText(results)
                
    def slotClicked(self):
        self.emit(QtCore.SIGNAL('do'))

    def expandColumn(self):
        print "[%s] Processing" % QtCore.QThread.currentThread().objectName()
        window.spot_data_widget.resizeColumnToContents(0)

    def print_(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("print_()"))
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'

    def handleLogin(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("handleLogin()"))
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'
   
    def cancel_process(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("cancelProcess()"))
        print "[%s] Cancel sent" % QtCore.QThread.currentThread().objectName()+'\n'

    def execute_NERSC_process(self):
        if (window.operation_box.currentText() == "Load Local Image"):
            self.openImageFinder()     
        # calls Downloader
        if (window.operation_box.currentText() == "Download SPOT Dataset to local drive"):
            self.emit(QtCore.SIGNAL("handleDownload()"))
            
        if (window.operation_box.currentText() == "Upload Local item to SCRATCH"):
            self.emit(QtCore.SIGNAL("handleUpload()"))
            
        if (window.operation_box.currentText() == "Run Tomopy2 Script (EXPERIMENTAL)"):
            self.emit(QtCore.SIGNAL("handleScript()"))
        # calls Worker
        else: 
            self.slotClicked()
            self.emit(QtCore.SIGNAL("handleProcess()"))
            print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'

    def cdls(self,path=""):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("cdls()"),)   
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'
        
    @QtCore.pyqtSlot(str, str)
    def createScripts(self, filename, mode):
        filename = filename.split('.')[0]
        if mode == "tomopy":
            self.writeExecutionWrapper("runTomopy_"+filename+".sh", "Tomopy_"+filename+".py")
            self.writeTomopy2Script("Tomopy_"+filename+".py", filename)
            self.directUploadSignal.emit("runTomopy_"+filename+".sh",rootpath+"runTomopy_"+filename+".sh")      
            self.directUploadSignal.emit("Tomopy_"+filename+".py",rootpath+"Tomopy_"+filename+".py")          
        if mode == "crop":
            self.writeExecutionWrapper("runCrop_"+filename+".sh", "Crop_"+filename+".py")
            self.writeTomopy2Script("Crop_"+filename+".py", filename)
            self.directUploadSignal.emit("runCrop_"+filename+".sh",rootpath+"runCrop_"+filename+".sh")      
            self.directUploadSignal.emit("Crop_"+filename+".py",rootpath+"Crop_"+filename+".sh")  

        return    
    
    def writeExecutionWrapper(self,Wrappername, pythonScript):
        writer = open(rootpath+Wrappername, 'w')
        writer.write('source /opt/modules/default/init/bash\n')
        writer.write('module load /usr/common/usg/python/2.7-anaconda\n')
        writer.write('source activate my_spew_conda\n')
        writer.write('python $SCRATCH/'+pythonScript+'\n')
        writer.write('source deactivate\n')
        writer.write('module unload python/2.7-anaconda\n')

        writer.close()
        return
    
    def writeTomopy2Script(self,scriptname, datasetName):
        writer = open(rootpath+scriptname, 'w')
        writer.write('# -*- coding: utf-8 -*-\n')
        writer.write('import tomopy\n')
        writer.write("import h5py\r\n")
        writer.write("import os\r\n")

        writer.write("scratchdir = os.environ['SCRATCH']\r\n")
        writer.write("fname = scratchdir"+'+\'/\'+'+"'"+datasetName+"'"+"+"+"'"+".h5"+"'"+"\r\n")
        writer.write("print fname\r\n")

        writer.write("proj, flat, dark = tomopy.io.exchange.read_aps_32id(fname)\r\n")
        writer.write("theta = tomopy.angles(proj.shape[0], 0, -180)\r\n")
        writer.write("rot_center = tomopy.find_center(proj, theta, emission=False, ind=0, init=1024, tol=0.5)\r\n")
        writer.write("print \"Calculated rotation center: \", rot_center\r\n")
        writer.write("rec = tomopy.recon(proj, theta, center=rot_center, algorithm='gridrec', emission=False)\r\n")
        writer.write("print 'create_folder: Will create ' + scratchdir+'/'+'recon'\r\n")
        writer.write("flag = os.system('mkdir '+ scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+")\r\n")
        writer.write("if (flag != 0):\r\n")
        writer.write("    print 'create_folder: Deleting ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+"\r\n")
        writer.write("    rm_flag = os.system('rm -r ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+")\r\n")
        writer.write("    error_by_flag(rm_flag, 'ERROR: cannot remove ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+")\r\n")
        writer.write("    rm_flag = os.system('rm -r ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+")\r\n")
        writer.write("    print 'do_reconstruction: Creating ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+"\r\n")
        writer.write("    flag = os.system('mkdir ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+")\r\n")
        writer.write("    error_by_flag(flag, 'ERROR: cannot create ' + scratchdir+'/'+'recon_'+"+"'"+datasetName+"'"+" + '. Delete folder manually.')\r\n")
        writer.write("tomopy.io.writer.write_tiff_stack(rec, fname=scratchdir+'/'+'recon_'+\'"+datasetName+"\'+'/'+'recon_'+\'"+datasetName+"\')\r\n")
        writer.close()

        return
    
    def writeCroppingScript(self,scriptname, datasetName):
        writer = open(rootpath+scriptname, 'w')
        writer.write('# -*- coding: utf-8 -*-\n')
        writer.write('import tomopy\n')
        writer.write("import h5py\r\n");
        writer.close()
        return

    def handleSCRATCHDirectoryList(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("handleSCRATCHDirectoryList()")) 
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'

    def handleSCRATCHAction(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("handleSCRATCHAction()"))
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'
     
    def handleSpotSearchRequest(self):
        self.slotClicked()
        self.spot_data_widget.clear()
        self.emit(QtCore.SIGNAL("handleSpotSearchRequest()"))    
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'

    def handleTreeView(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("handleTreeView()"))    
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'

    def handleTableView(self):
        self.slotClicked()
        self.emit(QtCore.SIGNAL("handleTableView()"))
        print "[%s] Tasks sent" % QtCore.QThread.currentThread().objectName()+'\n'
        
    """This is just a test function to check how images are loaded into the GUI"""  
    def openImageFinder(self):
        print "[%s] Processing" % QtCore.QThread.currentThread().objectName()
        fileName = QtGui.QFileDialog.getOpenFileName(window, "Open File",
                QtCore.QDir.currentPath())
        if fileName:
            image = QtGui.QImage(fileName)
            if image.isNull():
                QtGui.QMessageBox.information(self, "Image Viewer",
                    "Cannot load %s." % fileName)
                return
            _pixmap = QtGui.QPixmap.fromImage(image)
            scaled = _pixmap.scaled(window.width(), window.imageLabel.height(), QtCore.Qt.KeepAspectRatio)
            window.imageLabel.setPixmap(scaled)
            window.scrollArea.setWidgetResizable(True)
            #self.fitToWindowAct.setEnabled(True)
            #self.updateActions()
            window.imageLabel.adjustSize()

    """Function to fill SPOT datasets into the Treewidget"""  
    def fill_tree(self, item, value):
        item.setExpanded(False)
        #print(item,value)
        if type(value) is dict:
            for key, val in sorted(value.iteritems()):
                #print key,val
                child = QtGui.QTreeWidgetItem()
                child.setText(0, unicode(key))
                item.addChild(child)
                self.fill_tree(child, val)
        elif type(value) is list:
            for val in value:
                child = QtGui.QTreeWidgetItem()
                item.addChild(child)
                if type(val) is dict:      
                    child.setText(0, '[dict]')
                    self.fill_tree(child, val)
                elif type(val) is list:
                    child.setText(0, '[list]')
                    self.fill_tree(child, val)
                else:
                    child.setText(0, unicode(val))              
                child.setExpanded(False)
        else:
            child = QtGui.QTreeWidgetItem()
            child.setText(0, unicode(value))
            item.addChild(child)
            
    @QtCore.pyqtSlot(object, object)
    def fill_treeWidget(self, widget, value):
        widget.clear()
        self.fill_tree(widget.invisibleRootItem(), value)

    """Function to fill the metadata of SPOT datasets into the Tablewidget"""  
    def fill_table(self, item, value):
        item.clear()
        item.setRowCount(len(value))
        item.setColumnCount(2)
        item.setColumnWidth(0, 150);
        if type(value) is dict:
            for index,(key, val) in enumerate(sorted(value.iteritems())):
                #print (key,val)
                if type(val) is long:      
                    val = str(val)
                item.setItem(index, 0, QtGui.QTableWidgetItem(key))
                item.setItem(index, 1, QtGui.QTableWidgetItem(val))
        else:
            child = QtGui.QTableWidgetItem()
            child.setText(0, unicode(value))
            item.addChild(child)
            
    @QtCore.pyqtSlot(object, object)
    def fill_tableWidget(self, widget, value):
        widget.clear()
        self.fill_table(widget, value)

"""
Class for making a separate thread for remote script running
"""
class RemoteSH(QtCore.QObject):
    def __init__(self, parent):
        QtCore.QObject.__init__(self)
        self.parent = parent

    @QtCore.pyqtSlot(object)        
    def start(self):
        self.shprocess = ScriptProcess(self.parent)
        self.connect(self.parent, QtCore.SIGNAL('handleScript()'), self.shprocess.handle_script)
        print "[%s] start()" % QtCore.QThread.currentThread().objectName()+'\n'       
        self.shprocess.createScriptSignal.connect(self.parent.createScripts)
        self.shprocess.workingMessage.connect(self.parent.printMessage)

"""
Class that holds the functions used in the RemoteSH thread for remote script running
"""
class ScriptProcess(QtCore.QObject):

    workingMessage = QtCore.pyqtSignal(str)  
    createScriptSignal = QtCore.pyqtSignal(str,str)

    def __init__(self, parent):
        QtCore.QObject.__init__(self)   
        self.parent = parent 
        
    def cdls(self,path="", widget=None):
        global web_directory 
        if (path == "/file"):
            web_directory += "/edison"
        print "web_directory = "+web_directory
        self.clearSCRATCHSignal.emit()
        self.taskStart.emit()
        r = s.get(newt_base_url + web_directory)
        directory_items = r.json()
        for item in directory_items:
            if (item['name'] != '.'):
               widget.addItem(item['name'])
        directory_items = None
        self.taskDone.emit()
        
    def handle_script(self):
        if (window.operation_box.currentText() == "Run Script on SCRATCH (EXPERIMENTAL)"):
            item = window.scratch_data_widget.currentItem()
            if (item == None):
                self.workingMessage.emit("No script selected!")
                self.taskDone.emit()
                return
            filename = item.text()
            filename = str(filename)
            print "Attempting to run script: " +filename
            self.execute_script(path=filename)
            return  
            
        if (window.operation_box.currentText() == "Crop SCRATCH Dataset"):
            item = window.scratch_data_widget.currentItem()
            if (item == None):
                self.workingMessage.emit("No dataset selected!")
                self.taskDone.emit()
                return
            filename = item.text()
            filename = str(filename)
            print "Creating script to crop" +filename
            self.createScriptSignal.emit(filename, "crop")
            r = s.get(newt_base_url + web_directory)
            directory_items = r.json()
            for item in directory_items:
                if (item['name'] == "runCrop_"+filename.split('.')[0]+".sh"):
                    print "Running script to crop" +filename
                    self.workingMessage.emit("Running script to crop" +filename)

                    self.execute_script("runCrop_"+filename.split('.')[0]+".sh")
                    self.workingMessage.emit("Finished!")
                    print "Finished!"

            return      
            
        if (window.operation_box.currentText() == "Run Tomopy2 Script (EXPERIMENTAL)"):
            item = window.scratch_data_widget.currentItem()
            if (item == None):
                self.workingMessage.emit("No dataset selected!")
                self.taskDone.emit()
                return
            filename = item.text()
            filename = str(filename)
            print "Creating tomopy script for " +filename
            self.createScriptSignal.emit(filename, "tomopy")
            print "Running tomopy script for " +filename
            r = s.get(newt_base_url + web_directory)
            directory_items = r.json()
            for item in directory_items:
                if (item['name'] == "runTomopy_"+filename.split('.')[0]+".sh"):
                    print "Running tomopy recon for " +filename
                    self.workingMessage.emit("Running tomopy recon for " +filename)

                    self.execute_script("runTomopy_"+filename.split('.')[0]+".sh")
                    self.workingMessage.emit("Finished!")
                    print "Finished!"
            return          
        
    def execute_script(self, filename=""):
        print "executable:"+'/usr/bin/sh '+'/'+scratch_root+"/scratchdirs/"+str(window.user_box.text())+"/"+filename
        r = s.post(newt_base_url+"/command/edison", {"executable": '/usr/bin/sh '+'/'+scratch_root+"/scratchdirs/"+str(window.user_box.text())+"/"+filename})
        print r.text
        return

"""
Class for making a separate thread for uploading data onto SCRATCH
"""
class Uploader(QtCore.QObject):
    def __init__(self, parent):
        QtCore.QObject.__init__(self)
        self.parent = parent

    @QtCore.pyqtSlot(object)        
    def start(self):
        self.ulprocess = uploadProcess(self.parent)
        self.connect(self.parent, QtCore.SIGNAL('handleUpload()'), self.ulprocess.handle_upload)
        
        self.parent.directUploadSignal.connect(self.ulprocess.upload_data)
        self.ulprocess.taskDone.connect(self.parent.onFinished)
        self.ulprocess.uploadSignal.connect(self.parent.setDownloadRange)
        self.ulprocess.workingMessage.connect(self.parent.printMessage)
        self.ulprocess.clearSCRATCHSignal.connect(self.parent.scratch_data_widget.clear)
        self.ulprocess.taskStart.connect(self.parent.onStart)

        print "[%s] start()" % QtCore.QThread.currentThread().objectName()+'\n'       
"""
Class that holds the functions used in the Uploader thread for uploading data onto SCRATCH
"""  
class uploadProcess(QtCore.QObject):
    taskStart = QtCore.pyqtSignal()
    taskDone = QtCore.pyqtSignal()
    uploadSignal = QtCore.pyqtSignal(int)
    percentBarSignal = QtCore.pyqtSignal(int)
    workingMessage = QtCore.pyqtSignal(str)  
    percentTextSignal = QtCore.pyqtSignal(str)  
    clearSCRATCHSignal = QtCore.pyqtSignal()
    
    def __init__(self, parent):
        QtCore.QObject.__init__(self)   
        self.parent = parent 
        
    def cdls(self,path="", widget=None):
        global web_directory 
        if (path == "/file"):
            web_directory += "/edison"
        print "web_directory = "+web_directory
        self.clearSCRATCHSignal.emit()
        self.taskStart.emit()
        r = s.get(newt_base_url + web_directory)
        directory_items = r.json()
        for item in directory_items:
            if (item['name'] != '.'):
               widget.addItem(item['name'])
        directory_items = None
        self.taskDone.emit()
        
    def handle_upload(self):
        print "Uploading item!"
        global indexItem
        filename = window.fileSystemModel.fileName(indexItem)
        print filename
        filepath = window.fileSystemModel.filePath(indexItem)
        print filepath
        if (indexItem == None):
            self.workingMessage.emit("No item selected!")
            self.taskDone.emit()
            return
        #print filename
        self.workingMessage.emit("Uploading item!")
        self.upload_data(filename,filepath)
        self.taskDone.emit()
        self.parent.percentLabel.clear()
        print "Finished uploading item!"
        self.cdls(path=web_directory, widget=window.scratch_data_widget)

        self.workingMessage.emit("Finished uploading item!")
    
    @QtCore.pyqtSlot(str,str)        
    def upload_data(self, filename, filepath):
        global scratch_root
        print str(filename)
        if '.h5' in filename:
            f = open(str(filename), 'r')
            files = {'file': (filename, open(filepath, 'rb')), 'file_name': filename}
            r = s.post(newt_base_url+"/file/edison/"+"scratch2"+"/scratchdirs/mttang", files=files)
            r.content
        else:
            f = open(str(filename), 'r')
            files = {'file': f}
            r = s.post(newt_base_url+"/file/edison/"+scratch_root+"/scratchdirs/"+str(window.user_box.text()), files=files)
            r.content
        f.close()
        return
"""
Class for making a separate thread for downloading data from SPOT
"""        
class Downloader(QtCore.QObject):

    def __init__(self, parent):
        QtCore.QObject.__init__(self)
        self.stop_down = False
        self.parent = parent

    @QtCore.pyqtSlot(object)        
    def start(self):
        self.dlprocess = DownloadProcess(self.parent)
        self.connect(self.parent, QtCore.SIGNAL('handleDownload()'), self.dlprocess.handle_download)
        
        self.dlprocess.taskDone.connect(self.parent.onFinished)
        self.dlprocess.downloadSignal.connect(self.parent.setDownloadRange)
        self.dlprocess.percentBarSignal.connect(self.parent.onProgress)
        self.dlprocess.workingMessage.connect(self.parent.printMessage)
        self.dlprocess.percentTextSignal.connect(self.parent.printMessage)

        print "[%s] start()" % QtCore.QThread.currentThread().objectName()+'\n'
"""
Class that holds the functions used in the Downloader thread for downloading data from SPOT
""" 
class DownloadProcess(QtCore.QObject):
    taskDone = QtCore.pyqtSignal()
    downloadSignal = QtCore.pyqtSignal(int)
    percentBarSignal = QtCore.pyqtSignal(int)
    workingMessage = QtCore.pyqtSignal(str)  
    percentTextSignal = QtCore.pyqtSignal(str)  
    
    def __init__(self, parent):
        QtCore.QObject.__init__(self)   
        self.parent = parent 

    def handle_download(self):
        global url, stop_download
        print "Downloading dataset!"
        item = window.spot_data_widget.currentItem()
        if (item == None):
            self.workingMessage.emit("No dataset selected!")
            self.taskDone.emit()
            return
        filename = item.text(0)
        #print filename
        for key in my_data.keys():
            if key in filename and '/als/' not in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['raw']
                print url
                self.download_data(url)
                    
            if key in filename and '/raw/' in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['raw']
                print url
                self.download_data(url)
                    
            if key in filename and '/norm/' in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['norm']
                print url
                self.download_data(url)
                    
            if key in filename and '/gridrec/' in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['gridrec']
                print url
                self.download_data(url)
                
            if key in filename and '/imgrec/' in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['imgrec']
                print url
                self.download_data(url)
                
            if key in filename and '/rc/' in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['rc']
                print url
                self.download_data(url)
                
            if key in filename and '/sino/' in filename:
                self.workingMessage.emit("Downloading dataset!")
                url = "https://portal-auth.nersc.gov/als/hdf/download"+my_data[key]['sino']
                print url
                self.download_data(url)
                    
        stop_download = False
        self.taskDone.emit()
        self.parent.percentLabel.clear()
        print "Finished downloading dataset!"
        self.workingMessage.emit("Finished downloading dataset!")
    
    def download_data(self, url):
        #print "[%s] Processing download_data" % QtCore.QThread.currentThread().objectName()
        local_filename = url.split('/')[-1]
        r = s.head(url)
        head = r.headers
        file_size = int(head['content-length'])
        print "Downloading: %s Bytes: %d" % (local_filename, file_size)
        self.downloadSignal.emit(file_size)
        # NOTE the stream=True parameter
        #u = urllib2.urlopen(url)
        #meta = u.info()
        #file_size = int(meta.getheaders("Content-Length")[0])
        f = open(local_filename, 'wb')
        r = s.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            file_size_dl = 0
            percent_count = 1
            for chunk in r.iter_content(chunk_size=64*1024): 
                try:
                    if chunk: # filter out keep-alive new chunks
                        file_size_dl += len(chunk)
                        f.write(chunk)
                        status = r"[%3.2f%%]" % (file_size_dl * 100. / file_size)
                        if ((file_size_dl * 100. / file_size) >= percent_count) and (stop_download == False):
                            self.percentBarSignal.emit(percent_count)
                            percent_count += 1
                            self.workingMessage.emit("Downloading dataset!")
                            self.downloadSignal.emit(file_size)
                        f.flush()
                        self.percentTextSignal.emit(status)

                except StopIteration:
                    break
                if (stop_download == True):
                    print 'Killed from other thread!'
                    break
        r.close()
        f.close()
        return local_filename
        
class Worker(QtCore.QObject):

    def __init__(self, parent):
        QtCore.QObject.__init__(self)
        self.parent = parent

    def task(self, dataobj):
        print "[%s] Processing" % QtCore.QThread.currentThread(), dataobj
        sleep(3)
        print "Done with", dataobj
        self.emit(QtCore.SIGNAL("taskDone"), str(dataobj))
        
    @QtCore.pyqtSlot(object)
    def start(self):
        self.tasks = Tasks(self.parent)
       
        self.connect(self.parent, QtCore.SIGNAL('handleLogin()'), self.tasks.handle_login)
        self.connect(self.parent, QtCore.SIGNAL('handleProcess()'), self.tasks.NERSC_process)
        self.connect(self.parent, QtCore.SIGNAL("cancelProcess()"), self.tasks.cancel_process)        

        self.connect(self.parent, QtCore.SIGNAL('handleSCRATCHDirectoryList()'), self.tasks.handleSCRATCHDirectoryList)
        self.connect(self.parent, QtCore.SIGNAL('handleSCRATCHAction()'), self.tasks.handleSCRATCHAction)
        self.connect(self.parent, QtCore.SIGNAL('handleSpotSearchRequest()'), self.tasks.handleSpotSearchRequest)
        self.connect(self.parent, QtCore.SIGNAL('handleTreeView()'), self.tasks.handleTreeView)
        self.connect(self.parent, QtCore.SIGNAL('handleTableView()'), self.tasks.handleTableView)
    
        self.tasks.clearSCRATCHSignal.connect(self.parent.scratch_data_widget.clear)
        self.tasks.clearLOCALSignal.connect(self.parent.local_data_view.reset)
        self.tasks.clearSPOTSignal.connect(self.parent.spot_data_widget.clear)
    
        self.tasks.fillTableSignal.connect(self.parent.fill_tableWidget)
        self.tasks.fillTreeSignal.connect(self.parent.fill_treeWidget)
        self.tasks.fillLocalSignal.connect(self.parent.buildLocalDirectoryView)
    
        self.tasks.taskDone.connect(self.parent.onFinished)
        self.tasks.taskStart.connect(self.parent.onStart)
        self.tasks.downloadSignal.connect(self.parent.setDownloadRange)
        self.tasks.percentSignal.connect(self.parent.onProgress)
    
        self.tasks.workingMessage.connect(self.parent.printMessage)

        print "[%s] start()" % QtCore.QThread.currentThread().objectName()+'\n'

    def slotDo(self):
        print 'slotDo called in thread:', self.currentThread()
        self.emit(QtCore.SIGNAL('done'))
        
    def showWhatToDo(self, string):
        global command
        print string + "\n"
        self.emit(QtCore.SIGNAL(string))
        command = string

#    def run(self):
#        global command
#        self.exec_()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    QtCore.QThread.currentThread().setObjectName('main')
    window = MyApp()
    #scrollArea =  QtGui.QScrollArea()
    #scrollArea.setWidget(window)
    app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=False))
    window.show()
  
    sys.exit(app.exec_())