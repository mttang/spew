# README #

##How to use:##

Step 1. Download and install Anaconda Python 2.7: http://continuum.io/downloads

Step 2. Clone this repository with Git

Step 3. Open Anaconda command prompt. Go to cloned directory

Step 4. type pip install qdarkstyle

Step 5  type pip install requests

Step 6  Go to the this project's folder and type python spew.py

##How to setup on NERSC:##

Step 1. Get a NERSC account/SPOT account.

Step 2. Using Spew, upload the .yml file and "yml" script to your SCRATCH

Step 3. Execute the "yml" script

##How to edit:##

I highly recommend using Anaconda's Spyder IDE for editing the python script.
To adjust the .UI file. Use the QTdesigner provided by Anaconda (From your Anaconda directory: Anaconda\Lib\site-packages\PyQt4\designer).